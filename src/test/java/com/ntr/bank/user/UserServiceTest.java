package com.ntr.bank.user;

import com.ntr.bank.exception.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
@ActiveProfiles("test")
class UserServiceTest {

    @Autowired
    private UserService userService;

    @MockBean
    private UserRepository userRepository;

    @Test
    public void shouldRegisterUser() throws UserAlreadyExistException {
        //Arrange
        User user = new User();
        user.setEmail("toto");
        user.setPassword("pass");
        user.setBalance(0D);

        //Act
        userService.signUpUser(user);

        //Assert
        verify(userRepository).save(user);
    }

    @Test
    public void shouldDebitUser() throws UserNotFoundException, NotEnoughMoneyException, PasswordException, BadValueException {
        //Arrange
        User debitedUser = new User();
        debitedUser.setEmail("debitedUserEmail");
        debitedUser.setPassword("debitedUserPass");
        debitedUser.setBalance(10D);

        OperationRequest operationRequest = new OperationRequest();
        operationRequest.setEmail("debitedUserEmail");
        operationRequest.setPassword("debitedUserPass");
        operationRequest.setValue(5D);

        when(userRepository.findByEmail(debitedUser.getEmail())).thenReturn(debitedUser);

        //Act
        userService.debitUser(operationRequest);

        Double expectedBalance = 5D;
        Double userBalance = debitedUser.getBalance();

        //Assert
        assertThat(userBalance).isEqualTo(expectedBalance);
        verify(userRepository).save(debitedUser);
    }

    @Test
    public void shouldCreditUser() throws UserNotFoundException, NotEnoughMoneyException, PasswordException, BadValueException {
        //Arrange
        User creditedUser = new User();
        creditedUser.setEmail("debitedUserEmail");
        creditedUser.setPassword("debitedUserPass");
        creditedUser.setBalance(0D);

        OperationRequest operationRequest = new OperationRequest();
        operationRequest.setEmail("debitedUserEmail");
        operationRequest.setPassword("debitedUserPass");
        operationRequest.setValue(5D);

        when(userRepository.findByEmail(creditedUser.getEmail())).thenReturn(creditedUser);

        //Act
        userService.creditUser(operationRequest);

        Double expectedBalance = 5D;
        Double userBalance = creditedUser.getBalance();

        //Assert
        assertThat(userBalance).isEqualTo(expectedBalance);
        verify(userRepository).save(creditedUser);
    }

    @Test
    public void shouldThrowNotEnoughMoneyException_whenDebitUserWithBadValue() throws UserNotFoundException, BadValueException, PasswordException {
        //Arrange
        User debitedUser = new User();
        debitedUser.setEmail("debitedUserEmail");
        debitedUser.setPassword("debitedUserPass");
        debitedUser.setBalance(0D);

        OperationRequest operationRequest = new OperationRequest();
        operationRequest.setEmail("debitedUserEmail");
        operationRequest.setPassword("debitedUserPass");
        operationRequest.setValue(5D);

        when(userRepository.findByEmail(debitedUser.getEmail())).thenReturn(debitedUser);

        //Act Assert
        try {
            userService.debitUser(operationRequest);
            fail("Should throw not enough money exception");
        } catch (NotEnoughMoneyException e) {
        }
    }
}
