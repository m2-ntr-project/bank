package com.ntr.bank.user;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(UserController.class)
@ExtendWith(MockitoExtension.class)
@ActiveProfiles("test")
class UserControllerTest {

    @MockBean
    private UserController userController;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void shouldReturn201_whenRegister() throws Exception {
        //Arrange
        User user = new User();
        user.setEmail("email");
        user.setPassword("pass");
        user.setBalance(0D);
        user.setId(0L);
        String userJson = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(user);

        //Act & Assert
        mockMvc.perform(post("/users/register")
                .contentType(APPLICATION_JSON_VALUE)
                .content(userJson)
        ).andExpect(status().is2xxSuccessful());
    }

}
