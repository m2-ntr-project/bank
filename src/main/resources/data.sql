CREATE TABLE IF NOT EXISTS user_account (
    uid serial PRIMARY KEY,
    email VARCHAR (50) UNIQUE NOT NULL,
    password VARCHAR (50) NOT NULL
);
