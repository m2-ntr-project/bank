package com.ntr.bank;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;

@EnableSwagger2
@Configuration
@Profile({"!test","!prod"})
public class SwaggerConfig {

        @Bean
        public Docket api() {
            return new Docket(DocumentationType.SWAGGER_2)
                    .select()
                    .apis(RequestHandlerSelectors.basePackage("com.ntr.bank"))
                    .paths(PathSelectors.any())
                    .build();
        }

    private List<ResponseMessage> messagesForGetMethod() {
        List<ResponseMessage> messages = new ArrayList<>();
        messages.add(new ResponseMessageBuilder()
                .code(200).
                        message("Everything is fine")
                .responseModel(new ModelRef("OK"))
                .build());
        messages.add(
                new ResponseMessageBuilder()
                        .code(404).message("Resource is not available")
                        .responseModel(new ModelRef("Error"))
                        .build());
        messages.add(new ResponseMessageBuilder()
                .code(500)
                .message("Unexpected error")
                .responseModel(new ModelRef("Error"))
                .build());

        return messages;
    }
}
