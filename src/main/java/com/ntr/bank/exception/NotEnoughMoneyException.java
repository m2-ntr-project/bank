package com.ntr.bank.exception;

public class NotEnoughMoneyException extends Throwable {

    public NotEnoughMoneyException(){}

    public NotEnoughMoneyException(String message) {
        super(message);
    }

    public NotEnoughMoneyException(Throwable cause) {
        super(cause);
    }

    public NotEnoughMoneyException(String message, Throwable cause) {
        super(message, cause);
    }
}
