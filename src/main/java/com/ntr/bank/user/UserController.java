package com.ntr.bank.user;

import com.ntr.bank.exception.*;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/users")
@CrossOrigin(origins = "*")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    @ApiOperation(value = "Get all users", tags = "user")
    @GetMapping("/all")
    public ResponseEntity<Iterable<User>> getAllUsers() {
        Iterable<User> users = userRepository.findAll();
        return new ResponseEntity<>(users, HttpStatus.OK);
    }



    @ApiOperation(value = "Register", tags = "user")
    @PostMapping("/register")
    public ResponseEntity<String> register(@RequestBody User user) {
        try {
            userService.signUpUser(user);
        } catch(UserAlreadyExistException e) {
            return new ResponseEntity<>("Email " + user.getEmail() + " already exists !", HttpStatus.CONFLICT);
        }
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @ApiOperation(value = "debit a user", tags = "user")
    @PutMapping(value = "/debit",         consumes = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_XML_VALUE },
            produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_XML_VALUE })
    public ResponseEntity<String> debit(@RequestBody OperationRequest operationRequest) {
        try {
            userService.debitUser(operationRequest);
        } catch (NotEnoughMoneyException e) {
            return new ResponseEntity<>("Not enough money", HttpStatus.BAD_REQUEST);
        } catch (BadValueException e) {
            return new ResponseEntity<>("Value must not be null or negative", HttpStatus.PAYMENT_REQUIRED);
        } catch (UserNotFoundException | PasswordException e) {
            return new ResponseEntity<>("Wrong email or password", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>("User has been debited", HttpStatus.OK);
    }

    @ApiOperation(value = "credit a user", tags = "user")
    @PutMapping(value = "/credit",         consumes = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_XML_VALUE },
            produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_XML_VALUE })
    public ResponseEntity<String> credit(@RequestBody OperationRequest operationRequest) {
        try {
            userService.creditUser(operationRequest);
        } catch (BadValueException e) {
            return new ResponseEntity<>("Value must not be null or negative", HttpStatus.PAYMENT_REQUIRED);
        } catch (UserNotFoundException | PasswordException e) {
            return new ResponseEntity<>("Wrong email or password", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>("User has been credited", HttpStatus.OK);
    }

}
