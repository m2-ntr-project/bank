package com.ntr.bank.user;

import com.ntr.bank.exception.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public void signUpUser(User user) throws UserAlreadyExistException {
        String userEmail = user.getEmail();
        user.setBalance(0D);
        if(userRepository.existsByEmail(userEmail)) {
            throw new UserAlreadyExistException();
        }
        else {
            userRepository.save(user);
        }
    }

    public void debitUser(OperationRequest operationRequest) throws NotEnoughMoneyException, BadValueException, UserNotFoundException, PasswordException {
        User debitedUser = checkRequest(operationRequest);

        if((debitedUser.getBalance() - operationRequest.getValue()) >= 0) {
            debitedUser.setBalance(debitedUser.getBalance() - operationRequest.getValue());
            userRepository.save(debitedUser);
        }
        else {
            throw new NotEnoughMoneyException();
        }
    }

    public void creditUser(OperationRequest operationRequest) throws BadValueException, UserNotFoundException, PasswordException {
        User creditedUser = checkRequest(operationRequest);
        creditedUser.setBalance(creditedUser.getBalance() + operationRequest.getValue());
        userRepository.save(creditedUser);
    }

    private User checkRequest(OperationRequest operationRequest) throws UserNotFoundException, PasswordException, BadValueException {
        User creditedUser = userRepository.findByEmail(operationRequest.getEmail());
        if(creditedUser == null) {
            throw new UserNotFoundException();
        }

        if(!creditedUser.getPassword().equals(operationRequest.getPassword())) {
            throw new PasswordException();
        }

        if(operationRequest.getValue() < 0 || operationRequest.getValue() == 0) {
            throw new BadValueException();
        }
        return creditedUser;
    }
}
