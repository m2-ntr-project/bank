package com.ntr.bank.user;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OperationRequest {
    private String email;
    private String password;
    private Double value;
}
