### Run Bank API
Go into bank directory

- ` cd bank ` 

Run docker-compose 

- `docker-compose up`

### Run Front (ecommerce + gateway)
Go into front directory

- ` cd front `

Run docker build

- ` docker build -t front . `

Run docker container

- ` docker run -p 8080:8080 front `

Go to http://localhost:8080/ecommerce