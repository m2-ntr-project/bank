FROM tomcat:9.0

COPY ./target/bank-1.war /usr/local/tomcat/webapps/bank.war

EXPOSE 8081
CMD ["catalina.sh", "run"]
